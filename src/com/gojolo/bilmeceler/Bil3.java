package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Bil3 extends Activity {
	 static int count=0;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil3);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil3.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/8748899441");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    " Temel Ay�e'ye Ayakkab�m�n Tekini Ba�kas�nda G�rd�m Demi�. Ay�e Kimde Demi�. Temel Ayakkab�s�n�n Tekini Kimde G�rm��?",
	    " Adam�n Biri Gece Yar�s� Yata��n�n Etraf�nda Ko�uyormu� Ni�in? ",
        " minenin sana selam� var hangi mine",
        " fakirin tavu�u neden yumurtlamaz",
        " �ar�l �ar�l akan �elaleyi kim durdurur",
        " 1 adam 4.kattan atlam�� �lm�� ard�ndan diyeride 4. kattan atlam�� olmemi� neden",
        " Hangi Ay� K�� Uykusuna Yatmaz",
        " Yukar� bakar s�m��� akar :)) ",
        " �ki adam var 1 tanesi 4. kattan d���yor.Sonra di�eri atl�yor ama di�eri �lm�yor neden?",
        " hangi ay� k���n uyumaz*",
        " hangi arabayla seyhat edimez",
        " Y�rt�c� hayvana neyle bak�l�r",
        " En Ya�l� 10 Nedir ? ",
        " hangi kaz yenmez",
        " y�rt�c� bir hayvana nas�l bak�l�r",
        " hindi kalabal��a girmi� ne demi�",
        " Yar�m Bardak Suyun �st�ne Yar�m Bardak ��ki Konursa Ne Olur?",
        " Hangi Atan�n s�z�ne inan�lmaz?",
        " ezilen muz ne demi� ??",
        " eczaneye giren kedi ne al�r?",
        " Yatakta edilen dua'ya ne denir?",
        " k�pekler u�a bilseydi onlara ne ad verilirdi?",
        " Bir G�n Okulda Temele Sormu�lar Temele Demi�lerki Temel Sen Cennete Gitmek �stermisin Diye Temel Hay�r Demi� Neden ?",
        " ibonun sana selam� var.hangi ibo? ",
        " bir tavuk ayda ka� yumurta yapar?",
        " duru�u �m�r g�zleri k�m�r ",
        " bir kapta 5 bal�k varm�� 4 � bo�ulmu� ka� tane bal�k kalm��?",
        " ate� b�cegi ne zaman yanar ? ",
        " Day�m�n sana selam� var.Hangi Day�n?",
        " ate�b�ce�i ne zaman yanmaz"
	    };
	    final String[] cevaplar = {
	    	    "Di�er tekinde",
	    	    "Uykusu ka�m�� onu yakal�yormu�",
	            "��mine",
	            "sahibini ��martmamak i�in",
	            "Foto�raf makinesi",
	            "�LENLE �L�NMEZ",
	            "Oyuncak ay�",
	            "Mum tabi ki",
	            "�lenle �l�nmez :DDD",
	            "oyuncak ay�",
	            "el arabas�yla",
	            "G�zle",
	            "Karton",
	            "enkaz :)",
	            "uzaktan",
	            "bu ne kalabal�k bu ne kalabal�k :D",
	            "alttaki su sarho� olur",
	            "Salatan�n",
	            "muzlar konu�amaz",
	            "fare zehri",
	            "BEDdua",
	            "hav-u�",
	            "��nk� temelin annesi temele 'okuldan ��k�nca hemen eve gel' demi� :)",
	            "haribo",
	            "tavuk ayda yumurtlayamaz",
	            "kardan adam _",
	            "bal�k bo�ulmazki",
	            "elektrik faturas� �ok geldi�i zaman",
	            "depresyonday�m",
	            "elektrik faturas�n� �deyemedi�i zaman"
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
