package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Bil7 extends Activity {
	 static int count=0;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil7);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil7.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/5655832243");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    " Bir adam var mafyalar peşinde adam mağraya girmiş mağrada üç yol varmış ateş diken karanlik adam hangisinden geçer",
	    " Çiftçi Ali Amca sonbaharda 350 m2'lik bahçesini ekmeye başlar. Birinci gün bahçenin yarısını eker. İkinci gün kalan kısmın yarısını eker. Sizce Ali Amca her gün, kalan kısmın yarısını ekerek bahçeyi kaç günde bitirir?",
        " Sepetin içinde 8 elma var. Elmaları 8 çocuğa öyle bir şekilde paylaştırın ki her birine 1 elma düşsün, sepette de 1 elma kalsın. Nasıl?",
        " amerikada bir şöförsün 1.durakta 10 beyaz yolcu alıyorsun 2.durakta 5 beyaz yolcu indiriyorsun 5 siyahi yolcu alıyorsun 4.durakta hepsini indiriyorsun şöförün yaşı ve ayakkabı numarası kaçtır?",
        " 40 dakika boyunca yandığında tükenen iki mum var. Bu mumları kullanarak 30 dakikalık süreyi nasıl ölçeriz?",
        " Bir Gün Bir Temizlikçi Büyük Bir Gökdelen En Yüksek Katından Camları Silerken Yere Düşmüş. Ama Temizlikçi Ölmemiş. Nasıl? ",
        " 1, 2, 2, 2, 2 (1 ve 4 tane 2) rakamlarıyla, dört işlemi (+)(-)(x)(/) kullanarak 10 sayısını elde edebilir misin?",
        " 10 odalı bir butik otele 11 kişi geldi ve her biri ayrı oda istiyor. Otel görevlisi bu problemi çözdü: 11. kişiden 1. kişi ile birlikte bir süre 1 no'lu odada beklemesini istedi. 1 no'lu odada böylece 2 kişi vardı. Görevli 3. kişiyi 2 no'lu odaya, 4. kişiyi 3 no'lu odaya...., 10. kişiyi 9 no'lu odaya aldı. 1 no'lu odaya geri döndü ve 11. kişiyi 10 no'lu odaya aldı. Şimdi herkesin 1 odası oldu mu? ",
        " 10 ve 6 litrelik iki kabımız var. 8 litre suyu nasıl ölçeriz?",
        " Aşağıdaki şekli katlayarak küp haline getirdiğinizde birbirine zıt yüzlerin renkleri nasıl olur?",
        " Yolculuğa çıkan bir kadın bir yol ayrımına gelir. Varacağı kasabaya ulaşmak için sağ yolu mu yoksa sol yolu mu tercih edeceğini bilmemektedir. Yol ayrımında duran ikiz kardeşlere sormaya karar verir. Fakat kardeşlerden biri her zaman gerçeği söylerken diğeri her zaman yalan söylemektedir. Maalesef yolcu hangisinin doğru, hangisinin yalan söylediğini bilmiyor. Yolcu kardeşlerden birine sadece bir soru sorarak doğru yolu nasıl bulur?",
        " Hapishanedeki azılı iki mahkum bir hücrede tutulmaktadır. Hücrenin en tepesinde açık bir cam vardır. Mahkumlar üst üste çıksalar bile cama uzanamazlar. Kaçmak için tünel açmayı düşünürler fakat bu çok uzun süreceğinden vazgeçerler. Sonunda mahkumlardan biri kaçmak için yol bulur. Sizce mahkumun planı ne olabilir? Not: Odada üzerine çıkabilecekleri hiçbir eşya bulunmamaktadır. ",
        " Bir marangoz 10 metre uzunluğundaki kalası 1 dakika içinde kesmektedir. Bu kalası 10 eşit parçaya bölmesi için marangozun ne kadar zamana ihtiyacı vardır?",
        " İki saat önce saat, öğlen 1 ile gece saat 1'e eşit uzaklıkta idi. Şimdi saat kaçtır?",
        " Sadece 8, 8, 3, 3 rakamlarını kullanarak 24 sayısını nasıl elde edebilirim?",
        " Bir annenin yaşı kızının yaşının 3 katıdır. 12 yıl sonra annenin yaşı kızının yaşının 2 katı olacaktır. Anne ile kızının yaşları kaçtır?",
        " 1 kilometre uzunluğundaki bir trenin hızı dakikada 1 km ise, uzunluğu 1 km olan tünelden tamamen geçmesi ne kadar zaman alır?",
        " Bir itfaiyeci ve marangoz bankada sır bekliyorlar. Onlardan biri diğerinin oğlunun babası. Bu nasıl olabilir?",
        " Birkaç çocuğu olan bir aile biliyorum. Bu ailedeki erkek çocukların her birinin erkek kardeşleri kadar kız kardeşleri var fakat kız kardeşlerinin her birinin kız kardeşlerinin iki katı kadar erkek kardeşi var. Bu ailede kaç erkek kaç kız kardeş var?",
        " Çinliler neden Japonlardan daha fazla pirinç yer?",
        " Nisan ayında doğan kız kardeşimin doğum günü sonbaharda. Bu mümkün mü?",
        " Öyle iki sayı bulun ki aralarındaki fark 2 ve bu sayıların karelerinin arasındaki fark da 36 olsun.",
        " Suzan Hanım sekreterinden bir fincan kahve getirmesini ister. Sekreter kahveyi koyarken küpesi fincanın içine düşer fakat küpe ıslanmaz. Bu nası olur?",
        " Özel bir sayı var: Bu sayı rakamları toplamının 3 katıdır. Bu sayıyı bulabilir misin?",
        " Sütçünün 2 kabı var; kaplar 10 litrelik. Kaplardan birinde 9 litre süt, diğerinde ise 9 litre su var. Sütçü önce süt kabından 1 litre süt alarak su kabına ilave ediyor. Daha sonra su kabından 1 litre alarak süt kabına ilave ediyor. Bu durumda sütün içindeki su mu, yoksa suyun içindeki süt mü daha fazladır?",
        " Sonraki cümle yanlıştır. Önceki cümle doğrudur. Bu cümleler doğru mu yanlış mı?",
        " 6519428037 sayısı bir kurala göre oluşturulmuştur. Bu kural nedir?",
        " size üç yalan söylicem eğerbilirseniz size çikolata alcam:bi gemide bi yumurta varmış denize düşüp kırılmıştavuk da yumurtanın peşinden koşmuş üç yalan nelerdir",
        " Bir adama hem saatin kaç olduğunu, hem de ismini sormuşlar. Bu adam iki soruya da tek bir kelimeyle yanıt vermiş. Adamın söylediği bu kelime nedir?",
        " Dayının kız kardeşi teyzen değil ise neyin olur?"
	    };
	    final String[] cevaplar = {
	    	    "Adam dikeni alıp ateşe batırıp karanlıktan geçer",
	    	    "Hiç bitiremez.",
	            "Çocukların 7'si 1'er elma alır. 8. çocuk da 1 elma kalan sepeti alır.",
	            "bilmecenin başında şöförsün dediği için yaşını ve ayakkabı numaranı söylüyorsun",
	            "Birinci mumun iki ucunu da yakarım. İkinci mumun ise sadece bir ucunu yakarım. Birinci mum tükendiğinde 20 dakika geçmiş demektir. O anda ikinci mumun diğer ucunu da yakarım. İkinci mum tükendiğinde 10 dakika daha geçmiş olacak. Böylece 30 dakikayı ölçerim.",
	            "Çünkü temizlikçi camları içeriden siliyormuş",
	            "1) (2+2)x[2+(1/2)]=10 2) 2x2x2+2/1=10 3) 12-[(2+2)/2]=10",
	            "Hayır, bu imkansız. 2 no'lu odada 2. kişi olmalıydı.",
	            "10 litrelik kabı doldurup, 6 litrelik kaba boşaltırız. 6 litrelik kaptaki suyu bahçe sulamada veya diğer ev işlerinde kullanırız. 10 litrelik kapta kalan 4 litre suyu 6 litrelik kaba boşaltırız. 6 litrelik kapta hala 2 litre suya yetecek yer var. 10 litrelik kabı doldurur ve 6 litrelik kapta kalan 2 litrelik boşluğa tamamlarız. Böylece 10 litrelik kabımızda 8 litre su kalır.",
	            "Sarı-Mavi Kırmızı-Mor Yeşil-Pembe",
	            "Kadın kardeşlerden birine şu soruyu yöneltir: 'Kardeşine göre sol yol beni varacağım yere ulaştırır mı?' Eğer cevap evet olursa; yolcu sağ yolu tercih edecek, cevap hayır ise; sol yolu tercih edecek.",
	            "Önce tüneli kazmaya başlayacaklar ve çıkan toprak yığınının üzerine çıkıp cama ulaşacaklar.",
	            "9 dakika",
	            "Öğlen 1 ile gece 1 arasında 12 saat vardır. Yarısı 6 saattir. Her ikisinde de eşit uzaklıkta olan saat 7'dir. 7+2=9 olduğunda şimdi saat 9'dur.",
	            "√(8+8)x(3+3)=√16 x6=4x6=24",
	            "Kızın yaşı 12, annenin yaşı ise 36'dır.",
	            "2 dakika alır.",
	            "İtfaiyeci kadın ve marangoz erkek. Evliler ve oğulları var.",
	            "4 erkek, 3 kız kardeş var.",
	            "Çin'in nüfusu Japonya'nın nüfusundan fazla olduğu için.",
	            "Evet, kız kardeşim Avustralya'da yaşıyor.",
	            "8 ile 10",
	            "Henüz fincan boştu.",
	            "27",
	            "Suyun içindeki süt fazladır.",
	            "Ne doğru ne yanlıştır. Bu bir paradokstur. Eğer iki cümle doğru olsa ikinci cümle yanlış olurdu, bu durumda birinci cümlede yanlış olurdu. Bu ise imkansız.",
	            "Rakamlar yazılışlarına göre alfabetik olarak sıralanmışlardır; Altı, beş, bir, dokuz, dört, iki, sekiz, sıfır, üç, yedi",
	            "1 yumurta denize düşünce kırılamaz 2 tavuk koşamaz 3 bilsenizde size çikolata almicam",
	            "Durmuş. Adamın ismi Durmuş'tur. Aynı zamanda saatin kaç olduğunu da sordukları için kol saatinin de durmuş, yani çalışmıyor olduğunu söylemiştir.",
	            "Annen olur."
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
