package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Bil0 extends Activity {
 static int count=0;
	private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil0);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil0.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/2841966641");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    "Al�ac�k �i�man Sahibine D��man",
	    "Ufac�k Mermer Ta��, ��inde Beyler A��, Pi�irirsen A� Olur, Pi�irmezsen Ku� Olur.",
        "Ald�m bir tane, a�t�m bin tane",
        "Yer alt�nda k�rm�z� pancar.",
        " �ar��dan Al�nmaz Mendile Konulmaz Bundan Tatl� Bir�ey Olmaz",
        "Uzun kuyu dum dum suyu i�en �l�r i�meyen kal�r",
        "D�r�m D�r�m D�r�ld�m Ku�ak Gibi Sar�ld�m Karn�m� Yard�lar Benden Dolma Yapt�lar",
        " su i�ene kim dokunmaz",
        "Baldan Tatl� Baltadan A��r Elde Tutulmaz Mendile Konulmaz Tad�na Doyulmaz ",
        "yar�s� yenir yar�s� yenmez yar�s� gider yar�s� gitmez yar�s� yenir yar�s� yenmez nedir bu bil bakal�m",
        "G�ND�ZLER� S�NER GECELER� FENER",
        "z�p z�p z�plar ben vurunca daha �ok z�plar",
        "6 kara koyunun ka� aya�� vard�r",
        "z�plar z�plar yerinden oynar",
        "hangi koltuk alt�na k�rem s�r�lmez",
        "alfabenin ilk 4 harfi nedir",
        "�ncecik Beli Elimin Eli",
        "O Odan�n ��inde, oda, Onun ��inde.",
        "Sar� K�z Sark�p Durur D��cem Diye Korkup Durur",
        " mini mini k���k sini",
        " A�a�lar �i�ek A�ar t�m Hayvanlar Yavrular her Yer Ye�erir do�a �enlenir.",
        "benim bir suyum var d�kerim bizmez",
        "dokdor verdi ben i�tim iyile�tim",
        "Makarna ekiminin en y�ksek oldu�u �lke hangisidir �talyaFransaAmerika",
        "en zor avlanan hayvan hangisidir",
        "ba�a gittim bir ku� tuttum etini yedim kemi�ini att�m",
        "sesi var can� yok konu�ur a��z� yok",
        "bah�ede durur hi� k�p�rdamadan kargalar� kovalar",
        "tu�ladan yap�s� camdan kap�s�",
        "hanim icerde saci disarda"
	    };
	    final String[] cevaplar = {
	    	    "Pire",
	    	    "Yumurta",
	    	    "Nar",
	    	    "Turp",
	    	    "Uyku",
	    	    "t�fenk",
	    	    "lahana",
	    	    "y�lan dokunmaz",
	    	    "uyku",
	    	    "mandalina arabas�",
	    	    "YILDIZ",
	    	    "top",
	    	    "4 ��nk� koyunun alt� karad�r",
	    	    "kurba�a",
	    	    "kanepenin alt�na",
	    	    "alfa",
	    	    "�atal",
	    	    "Ayna",
	    	    "kay�s�",
	    	    "mercimek",
	    	    "ilkbahar",
	    	    "�elale",
	    	    "ila�",
	    	    "Makarna ekilmez ki...",
	    	    "dinozor",
	    	    "�z�m",
	    	    "radyo",
	    	    "korkuluk",
	    	    "ev",
	    	    "m�s�r"
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
