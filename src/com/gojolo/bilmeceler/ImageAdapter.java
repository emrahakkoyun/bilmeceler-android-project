package com.gojolo.bilmeceler;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    
    // Keep all Images in array
    public Integer[] mThumbIds = {
    		R.drawable.bil1,
    		R.drawable.bil2,
    		R.drawable.bil3,
    		R.drawable.bil4,
    		R.drawable.bil5,
    		R.drawable.bil6,
    		R.drawable.bil7,
    		R.drawable.bil8,
    		R.drawable.bil9,
    		R.drawable.bil10,
    };
 
    // Constructor
    public ImageAdapter(Context c){
        mContext = c;
    }
 
    @Override
    public int getCount() {
        return mThumbIds.length;
    }
 
    @Override
    public Object getItem(int position) {
        return mThumbIds[position];
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(mThumbIds[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(340,340));
        return imageView;
    }
}
