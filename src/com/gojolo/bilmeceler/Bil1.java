package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Bil1 extends Activity {
	 static int count=0;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil1);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil1.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/4318699842");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    "Yapra�� var,dal� yok,ortas� var g�vdesi yok",
	    "karaca giyer ferace i�er t�t�n� s�rer sefas�n�",
        "Al�ac�k �i�man Sahibine D��man",
        "Pencereden Ay Do�du G�renler Hayran Oldu Annesi Daha Do�madan K�z�n�n K�z� Oldu",
        "��i Ta� D��� Ta� Ha Dola� Ha Dola�",
        "Kara koyun meler ge�er Da�� ta�� deler ge�er",
        "AH NE �D�M NE �D�M SAMUR K�RKL� BEY �D�M",
        "marul sever hemen yer tarlada bulunur s�r�n�r gider",
        "��t demeden �al�ya girer ?",
        "Biz biz bizidik y�z on tane k�zd�k gece oldu d�z�d�k sabah oldu s�z�ld�k",
        " 9 LA TAVUS KU�U ARASINDA NE BENZERL�K VAR",
        "insan v�cudunda inip ��kan �ey nedir",
        "didik didik aran�r keyfinden ka�maz baz�lar� hi� yakalanmaz",
        "�K� YILDIZ G�ZLER� BOYNUZ",
        "B�lb�ll�ce ku� dillice.A�z� yok burnu sivrice",
        "Ben dururum o gider.Bil bakal�m bu ne ?",
        "bil bil bilmece da� �st�nde gezmece?",
        "Ufac�k Mil Ta�� Dolan�r Da�� Ta��",
        "bir �ocugum var �apkal�",
        "�ST� YE��L A�A� DE��L YANLARI KIRMIZI DOMATES DE��L ��� BEYAZ PEYN�R DE��L",
        " kuyu, kuyunun i�inde su suyun i�inde y�lan y�lan�n az�nda duman",
        "z�playarak y�r�r, patlayarak �l�r",
        "Ye�il yaprakl� k�rm�z� yanakl�",
        " ye�ilken dalda durur, sarar�r yerde durur",
        "z�playarak y�r�r, patlayarak �l�r",
        "y�r�r y�r�r hi� yorulmaz acaba ne",
        "yaz�n s�nmez k���n g�r�lmez",
        "Yaz�n �f P�f Der �ikayet Ederiz K���n Onu �ok Severiz",
        "�st� Ye�il A�a� De�il,yanlar� K�rm�z� Domates De�il,i�i Beyaz Peynir De�il, Nedir Bu",
        "SON S�Z�M�Z O OLSUN, RUHUMUZ NURLARLA DOLSUN"
	    };
	    final String[] cevaplar = {
	    	    "DEFTER",
	    	    "baca",
	            "Pire",
	            "�i�ek",
	            "Minare",
	            "TREN",
	            "KESTANE",
	            "Salyangoz",
	            "G�ne�",
	            "Y�ld�z",
	            "�K�S�N�N DE KUYRU�U KES�L�NCE DE�ER� KALMAZ",
	            "tansiyon",
	            "h�rs�z",
	            "SALYANGOZ",
	            "NOHUT",
	            "SES",
	            "k�zak",
	            "g�z",
	            "�ivi",
	            "TURP",
	            "gaz lambas�",
	            "kurba�a",
	            "K�RAZ",
	            "yaprak.",
	            "pire.",
	            "g�lge",
	            "g�ne�",
	            "G�NE�",
	            "turp",
	            "KEL�ME-� TEVH�D"
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
