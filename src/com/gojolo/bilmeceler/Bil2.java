package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Bil2 extends Activity {
	 static int count=0;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil2);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil2.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/5795433048");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    "Korkma Ge�mi�inden Ka�ma Gelece�inden",
	    "Elle tutulmaz g�zle g�r�lmez",
        "G�nd�zleri S�ner Geceleri",
        " kanad� vaar ku� de�il boynuzu var ko� de�il bil bakal�m ne",
        "ben giderim , o kal�r",
        "bir direk ustunde bir cati",
        "hi� su�um yokken durmadan vuruyorlar?",
        "1 Tabut ��inde 40 �l�",
        "Tarladan Ald�m Ye�il �ar��dan Ald�m Siyah Evde K�rm�z�",
        "sar�d�r sarpan gibi yaz�s� kuran gibi",
        "sar�d�r sallan�r dal�nda ballan�r",
        "s�ca�a koyma kurur suya kotma k�p�r�r",
        "sarmaca solmaca a�ca dolmaca",
        " sar�d�r safran gibi okunur kuran gibi ya bunu bileceksin yada bu gece �leceksin",
        " z�rafa kadar uzun, z�rafa kadar h�zl�, ama z�rafa kadar a��r olmayan �ey nedir?",
        " �st�ne yaz�lmayan sat�r hangisidir.",
        " mavi tarla �st�nde beyazg�vercin y�r�r",
        " Kat Katt�r Katmer De�il K�rm�z�d�r Biber De�il",
        " iki �ubuk bir makas hakkabaz m� hakkabaz",
        " rengidir gri k���c�kt�r bebe�i ",
        " vurulsun vurulsun g�z�nden yaz akmas",
        " Zilim Var Kap�m Yok Konu�urum A�z�m Yok ?",
        " uzald�k�a k�salan �ey nedir",
        " gelir leyla gider leyla tek ayak �st�nde durur leyla",
        " ya�mur ya�arken y�kselen �ey nedir",
        " hep �n�ndedir ama g�remessin?",
        " Zilim Var Kap�m Yok Konu�urum A�z�m Yok ",
        " yaz�n yatar k���n kalkar ",
        " ben giderim o gider g�ne�te beni izler",
        " �spanyollar Neden Bellerine K�m�z� Ku�ak Ba�larlar? "
	    };
	    final String[] cevaplar = {
	    	    "ecel",
	    	    "r�zgar",
	            "Y�ld�zlar",
	            "kelebek",
	            "ayak izi",
	            "semsiye",
	            "top",
	            "K�BR�T",
	            "�ay",
	            "alt�n",
	            "portakal",
	            "sabun",
	            "y�lan",
	            "alt�n",
	            "Z�RAFANIN G�LGES�",
	            "kasap sat�r� .",
	            "bulut",
	            "G�l",
	            "leylek",
	            "fil",
	            "davul",
	            "Telefon",
	            "hayat",
	            "kap�",
	            "�emsiye",
	            "gelecek",
	            "telofon",
	            "soba",
	            "g�lge",
	            "pantolonlar� d��mesin diye"
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
