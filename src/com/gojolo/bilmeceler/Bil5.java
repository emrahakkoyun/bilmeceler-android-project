package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Bil5 extends Activity {
	 static int count=0;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil5);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil5.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/2702365849");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    " filin derisi neden buru�uktur?",
	    " T�r filim �ekerse nolur ",
        " Berat ad�nda biri okula kazan getirmi� neden",
        " her �eye maydonoz olan adam� ne yaparlar",
        " Hangi Mevsim Daha ��kt�r?",
        " Portakal�n tersi nedir?",
        " bir japon bir japondan nas�l su ister",
        " casusluk yapan deftere ne denir ",
        " t�r bir filmde oynarsa ne olur",
        " en ya�l� say� nedir",
        " �ay ile Nescafe aras�nda ne fark vard�r?",
        " Erkek ata ne denir??",
        " Bir adam denize d��m�� ama bo�ulmam��.Neden? ",
        " En h�zl� on",
        " bir bebek arabas�nda ate�le yakla�may�n yaz�yormus acaba neden? ",
        " ternoeyi metre nin d��mesi neyi g�sterir ",
        " Bal�k tutan 2 adam varm��,birinin a��n�n ipi varm��.birinin yokmu� ipi olan ipi olmayana ne demi�? ",
        " yar�m bardak suyun �st�ne i�ki eklersek alttaki su ne ollur",
        " Yatakta edilen duaya ne denir??",
        " y�lan ile kirpi evlenince ne olur? ",
        " vin� arabaya ne der ",
        " tem oto yoluna muz d��erse ne olur?",
        " Vezir sadece nereye tek ba��na gider? ",
        " Hangi Bah�ede �i�ek Yeti�mez",
        " Hakim ile hakem aras�nda ne fark vard�r?",
        " hangi fen uyar�dir",
        " Hangi Ata i�ilir ",
        " hangi �rt� masaya �rt�lmez",
        " efe sizi sordu hangi efe",
        " hangi at ger�ek deildir"
	    };
	    final String[] cevaplar = {
	    	    "�t�lenmedi�i i�in",
	    	    "T�rakt�r",
	            "dersi kaynatmak i�in",
	            "salataya do�ram��lar",
	            "KI� (TERSTEN OKUNUNCA)",
	            "Portagit",
	            "matarama su ko ::D",
	            "ajanda",
	            "t�rakt�r",
	            "dokuz (nine-�ngilizce)",
	            "Nescafede y�z�lmez",
	            "BAYat",
	            "��nk� adam�n tipi kay�km��",
	            "Jeton",
	            "bebek t�p bebek oldu�u i�in:DDD",
	            "�ivinin iyi �ak�lmad���",
	            "Kablosuz a��n �ifresi ne?",
	            "sarho� olur",
	            "BEDdua :D",
	            "dikenli tel",
	            "g�l�mse �ekiyorum",
	            "temmuz",
	            "Tuvalete",
	            "fenerbah�e'de",
	            "Hakim i�eri atar hakem d��ar� atar",
	            "l�tfen",
	            "limonata",
	            "bitki �rt�s�",
	            "k�nefe",
	            "sant�ran� at�"
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
