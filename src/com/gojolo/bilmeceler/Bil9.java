package com.gojolo.bilmeceler;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Bil9 extends Activity {
	 static int count=0;
		private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bil9);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(Bil9.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/2562765048");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
	    Button btn1 = (Button)findViewById(R.id.btn1);
	    Button btn2 = (Button)findViewById(R.id.btn2);
	    Button cevap = (Button)findViewById(R.id.cevap);
	  final  TextView text1 = (TextView)findViewById(R.id.text1);
	  final  TextView text2 = (TextView)findViewById(R.id.text2);
	    final String[] sorular = {
	    "�l�m cezas�na �arpt�r�lan bir adama son s�z� sorulmadan �nce ��yle deniliyor. Son s�z�n� yalan s�ylersen as�larak, do�ru s�ylersen kesilerek �leceksin. Bu adam�n kurtulma �ans� oldu�una g�re �l�m cezas�ndan kurtulmak i�in ne s�ylemelidir?", 
	    "�� su�lu ayn� su�a kat�lmaktan yarg�lanmaktad�r. Su�u an a��r olan giyotinle idam edilecektir. �� su�ludan biri olan A avukat�ndan �u cevab� al�r. � Bildi�im bir �ey varsa B ye �l�m cezas� verilmeyecektir. C nin B den daha a��r bir su� i�ledi�i kesin. Senin dosyan ise hen�z incelenmedi. Bu bilgiler �����nda A ya �l�m cezas� verilme ihtimali nedir?",
        "Bir mahkumun �l�m cezas� kuraya ba�lanm��t�r. Kuray� haz�rlayan vezir mahkumun az�l� d��man� oldu�u i�in iki ka��da da �l�m yazacakt�r. Onun b�yle yapaca��n� mahkum da bilmektedir. Fakat sultan�n yan�nda veziri itham edememektedir. Mahkum nas�l hareket etmeli ki �l�mden kurtulsun?",
        "Adam�n biri yan�nda bir kurt,bir ke�i ve kocaman bir ba� lahana ile bir nehri ge�mek ister. Ne var ki adam�n elinde bulunan kay�k ancak, kendisi ile birlikte bu �� �eyden yaln�zca birisini ta��yacak kapasitededir. Adam yan�nda lahanay� g�t�rse kurt kuzuyu yiyecek, kurdu g�t�rse kuzu lahanay� yiyecektir. lahana ile ke�inin g�venli�i ise adamla birlikte olmaya ba�l�d�r. Adam nas�l yapmal� ki hem kar��ya ge�sin, bu �� �eyi de beraber ge�irsin, ayn� zamanda hi� birine bir zarar gelmesin. Siz olsayd�n�z ne yapard�n�z.",
        "�ki basamakl� �yle bir say� bulunuz ki, buldu�unuz bu say�n�n rakamlar�n� �nce birbiri ile �arp�n�z, sonra toplay�n�z. Buldu�unuz �arp�m sonucu ile, toplam sonucunu tekrar toplay�n�z. Sonu�ta ilk yazd���n�z iki basamakl� say�y� versin.",
        "Elinizde sadece zaman ayarlay�c� olarak 2 tane kum saati vard�r. Bunlardan biri 7 dakikal�k, di�eri de 11 dakikal�kt�r. Yapmay� d���nd���n�z deney tam 15 dakika s�rmektedir. E�er zaman� uzat�rsan�z veya k�salt�rsan�z deneyde istedi�iniz verimi alamamaktas�n�z. Bu iki kum saatini kullanarak bu deneyin zaman ayarlamas�n� nas�l yapars�n�z."
	    };
	    final String[] cevaplar = {
	    	    "Beni asarak �ld�receksiniz derse kurtulur. ��nk�, onu asmaya g�t�rseler do�ru s�ylemi� olur, o zaman ba�� kesilmesi gerekir. Ba��n� kesmeye g�t�rseler, o zaman yalan s�ylemi� olacak ve as�lmas� gerekecek. B�ylece adam ne as�labilir ne de kesilebilir.",
	    	    "Su�un a��rl��� s�ras�na g�re 6 �e�it dizili� m�mk�nd�r. ABC ACB BAC BCA CBA CAB Avukat B nin en a��r su�lu olmad���n� belirtmi�tir. O halde BAC ve BCA m�mk�n de�ildir. C nin B den daha su�lu olmas� gerekti�inden, ABC su� s�ras� da olamaz. Geriye, ACB, CAB, CBA kal�r. Bunlardan yaln�z birinde A en ba�ta (yani en su�lu) oldu�undan A ya giyotin cezas� verilme ihtimali 1/3 t�r.",
	            "�ekti�ime raz�y�m deyip �ekti�i ka��d� yutmal�. Geriye kalan ka��tta �l�m yazd��� i�in yuttu�u ka��t sa� kald���n� ifade eder.",
	            "Kurt lahana yemedi�i i�in adam �nce ke�iyi kay��� ile kar��ya ge�irir. Ke�iyi b�rak�p kendisi yaln�z ba��na d�ner. Lahanas�n� al�p ke�inin yan�na gider. Lahanay� orada b�rak�p ke�i ile geri d�ner. Ke�iyi b�rak�p kurdu kay���na al�r onu kar��da b�rak�r. Kurt lahana ile kal�r. Kendisi yaln�z ba��na d�ner. Ke�iyi al�r beraberce kar��ya ge�erler. B�ylece ��� de ge�mi� olur.",
	            "Diyelim ki tuttu�umuz say� 49 olsun 1. i�lem: 4 x 9 = 36 2. i�lem: 4 + 9 = 13 ���nc� i�lem = 36 + 13 _ 49 .Sorunun cevab� sonu 9 ile biten 19, 29, 39, 49, 59, 69, 79, 89, 99 say�lar�ndan herhangi biri olabilir.",
	            "�ki kum saatini ayn� anda ba�lat�rs�n�z. 7 dakikal�k kum saati bo�ald��� anda deneye ba�lars�n�z. 11 dakikal�k bo�ald��� anda tekrar ters �evirirsiniz. Buraya kadar 4 dakika ge�mi�tir. 11 dakikal�k kum saati bo�ald��� anda deneyi sona erdirirsiniz. B�ylece toplam 15 dakika olmu�tur."
	    	    };
	    text1.setText(sorular[count].toString());
         btn1.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			    	count++;
				text2.setText("");
				text1.setText(sorular[count].toString());
			}
		});
	    btn2.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
			    	count--;
				text2.setText("");
				text1.setText(sorular[count].toString());
				
			}
		});
	    
	   cevap.setOnClickListener(new  View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				text2.setText(cevaplar[count].toString());
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
